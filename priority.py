"""
Script that can be used to prioritise voluntary humanitarian mapping campaigns. 
Locations should be prioritised where changes in variable being measured are
largest at current dataset coverage level.

In this script nmetwork distances between population and healthcare points for 27 
countries are repeatedly measured on an increasingly degraded OpenStreetMap road and 
path network. 

Postgis database (https://postgis.net/) used to store OSM healthcare facility 
locations and OSM road and path network. Loaded using osm2po (https://osm2po.de/)
and osm2pgsql (https://osm2pgsql.org/) 

pgrouting (https://pgrouting.org/) used to find shortest path using the 
dijkstra shortest path algorithm 

Population data: https://hub.worldpop.org/ 

"""

from fiona import open as fio_open
from rasterio import open as rio_open
from shapely.geometry import mapping, Point
from shapely import wkt
from math import ceil, floor
from numpy.random import uniform
from geopandas import read_file
from pandas import read_csv
from pandas import DataFrame as DataFrame
import psycopg2 as pg
from pyproj import Geod
from urllib.request import urlretrieve
from multiprocessing import Pool


def routing(coords):
    
    """
    Find distance between two points (here a population point and a healthcare facility)
    using the underlying road network 

    """
    
    #connect to the database
    connection = pg.connect(database="routing_world", user="postgres", password="", host="127.0.0.1", port="5432")
    cur = connection.cursor()
    
    #initialise lists and variable to store various results of routing
    distances = []
    comparison = []
    network = 0 

    #Loop through coordinates
    for coord in coords:

        #Find nearest node to population point
        cur.execute("select id, st_astext(the_geom) from wo_2po_4pgr_vertices_pgr order by the_geom <-> 'SRID=4326;POINT(" + str(coord[0]) + " " + str(coord[1]) + ")' limit 1;")    
        pop_points = cur.fetchall()[0]
        pop_point = pop_points[0]
        population = wkt.loads(pop_points[1])

        #Find the nearest healthcare centre to the population point 
        cur.execute("select st_astext(way), name from planet_osm_point order by way <-> 'SRID=4326;POINT(" + str(coord[0]) + " " + str(coord[1]) + ")' limit 1;")
        healthcare = cur.fetchall()[0]
        healthcare_point = wkt.loads(healthcare[0])

        try:

            #Find component of the graph containing the population and healthcare centre nodes
            cur.execute("select component from (select * from pgr_connectedComponents('SELECT id, source, target, cost, reverse_cost FROM wo_2po_4pgr WHERE (target_2 IS NOT NULL) AND (source_2 IS NOT NULL) AND (geom_way && (select st_envelope(st_buffer(st_geomfromtext(''LINESTRING(" + str(coord[0]) + " " + str(coord[1]) + "," + str(healthcare_point.x) + " " + str(healthcare_point.y) + ")''), 1)) as myarea))')) as a where a.node =" + str(pop_point) + ";")
            component = cur.fetchall()[0]
            # print(component)

            #Find node nearest routable node to the healthcare centre in the component
            cur.execute("select id from wo_2po_4pgr_vertices_pgr where id in (select node from (select * from pgr_connectedComponents('SELECT id, source, target, cost, reverse_cost FROM wo_2po_4pgr WHERE (target_2 IS NOT NULL) AND (source_2 IS NOT NULL) AND (geom_way && (select st_envelope(st_buffer(st_geomfromtext(''LINESTRING(" + str(coord[0]) + " " + str(coord[1]) + "," + str(healthcare_point.x) + " " + str(healthcare_point.y) + ")''), 1)) as myarea))')) as a where a.component = " + str(component[0]) + ") order by the_geom <-> 'SRID=4326;POINT(" + str(healthcare_point.x)+ " "  + str(healthcare_point.y) + ")' limit 1;")
            healthcare_node = cur.fetchall()[0]
            # print(healthcare_node)               

            #Get the coordinates of the nearest routable node 
            cur.execute("select st_astext(the_geom) from wo_2po_4pgr_vertices_pgr where id in (select node from (select * from pgr_connectedComponents('SELECT id, source, target, cost, reverse_cost FROM wo_2po_4pgr WHERE (target_2 IS NOT NULL) AND (source_2 IS NOT NULL) AND (geom_way && (select st_envelope(st_buffer(st_geomfromtext(''LINESTRING(" + str(coord[0]) + " " + str(coord[1]) + "," + str(healthcare_point.x) + " " + str(healthcare_point.y) + ")''), 1)) as myarea))')) as a where a.component = " + str(component[0]) + ") order by the_geom <-> 'SRID=4326;POINT(" + str(healthcare_point.x)+ " "  + str(healthcare_point.y) + ")' limit 1;")
            node_geom = cur.fetchall()[0]
            node_point = wkt.loads(node_geom[0])
            
            #Route between the population node and nearest routable node to healthcare centre
            cur.execute("SELECT sum(b.cost), st_astext(st_union(b.geom_way)) FROM pgr_dijkstra('SELECT id, source, target, cost from wo_2po_4pgr WHERE geom_way && (select st_envelope(st_buffer(st_geomfromtext(''LINESTRING("+ str(coord[0]) + " " + str(coord[1]) + "," + str(healthcare_point.x) + " " + str(healthcare_point.y) + ")''), 1)) as myarea)'," + str(pop_point) + "," + str(healthcare_node[0]) + ") as a left join wo_2po_4pgr b on b.id = a.edge;")
            cost = cur.fetchall()

        # Except no route can be found
        # Calculate straight line distance instead
        except Exception as e: 

            #Add straight line distance to list of distances
            distances.append(g.inv(population.x, population.y, healthcare_point.x, healthcare_point.y)[2])

            #Skip to next set of coords
            continue

        #Work out distance between the nearest routable node and the healthcare centre 
        extra_distance = g.inv(node_point.x, node_point.y, healthcare_point.x, healthcare_point.y)[2]
        
        #Work out straight line distance 
        straight_distance = g.inv(population.x, population.y, healthcare_point.x, healthcare_point.y)[2]
        
        #If a value is returned from routing
        if len(cost) > 0:
            
            #Add counter to number of network routes calculated
            network += 1

            try:
                #add network route plus any extra distance to distance list
                distances.append(cost[0][0] + extra_distance)
                
                #add network route plus extra distance and straight distance to distance comparison list
                comparison.append([cost[0][0] + extra_distance, straight_distance])                   

                #Load route as a geometry
                shape = wkt.loads(cost[0][1])                    
                
                #Write route to shapefile
                o.write({'geometry': mapping(shape), 'properties': {'degradation': int(completeness)}})

            except:
                #if exception add straight distance to distance list
                distances.append(straight_distance)    

        else:
            #Add straight distance to list of distances
            distances.append(straight_distance)

    #close database collection and return
    connection.close()
    return distances, network, comparison
        
def count_network(c, bounds):

    #create new database connection
    connection = pg.connect(database="routing_world", user="postgres", password="", host="127.0.0.1", port="5432")
    cur = connection.cursor()

    #need to intersect with the country bounds 
    cur.execute("SELECT COUNT(*) FROM wo_2po_4pgr WHERE (class2 = '" + c + "') AND (ST_Intersects(geom_way, ST_SetSRID(ST_GeomFromText('" + str(bounds) + "'), 4326)));")
    count = cur.fetchall()[0]
    connection.commit()

    #close database connection
    connection.close()

    return count[0]

def degrade_values(c, bounds, degradationVal):

    #create new database connection
    connection = pg.connect(database="routing_world", user="postgres", password="", host="127.0.0.1", port="5432")
    cur = connection.cursor()

    #need to intersect with the country bounds 
    cur.execute("SELECT COUNT(*) FROM wo_2po_4pgr WHERE (class2 = '" + c + "') AND (ST_Intersects(geom_way, ST_SetSRID(ST_GeomFromText('" + str(bounds) + "'), 4326)));")
    count = cur.fetchall()[0]
    degrade_value = floor((count[0]/100) * degradationVal)

    #Randomly select ways from the class
    #Where  source and target NOT NULL
    #And intersects with the country bounds
    #And set their source and target to null
    cur.execute("UPDATE wo_2po_4pgr SET target_2 = NULL, source_2 = NULL where id IN(SELECT id FROM wo_2po_4pgr WHERE (class2 = '" + c + "') AND (ST_Intersects(geom_way, ST_SetSRID(ST_GeomFromText('" + str(bounds) + "'), 4326))) AND (source_2 IS NOT NULL) AND (target_2 IS NOT NULL) order by random() limit " + str(degrade_value) + ");")
    connection.commit()

    #close database connection
    connection.close()

    return degrade_value



NUM_THREADS = 4

#Set geoid as WGS1984 ellipsoid for distance calculations
g = Geod(ellps='WGS84') 

# load the countries data into a GeoDataFrame
world = read_file('./data/ne_50m_admin_0_countries.shp')

# Read in the csv file countaining the countries of interest
country_list = read_csv('./data/countries.csv')

#Set up list of way classes needed to degrade the dataset
classes = ['primary', 'secondary', 'tertiary', 'unclassified', 'path']

#set up various lists to store results
country_results = []

#Set up list to stop same imagery being downloaded multiple times 
imagery = []

#set degradation value 
degradationVal = 10

#needed for threading
if __name__ == '__main__':
    
    # loop through each country
    for index, row in country_list.iterrows():
    
        #print country name
        print(row['Name'])
        
        #Get ISO codes of the country
        ISO_code = row['ISO_code']
        ISO_code_l = row['ISO_low']
    
        #Check if you already have it in your file before downloading!
        source = row['imagery']

        # get pop data
        # Commented out as all data was downloaded but can be used 
        filepath = 'https://data.worldpop.org/GIS/Population/Global_2000_2020_Constrained/2020/' + source +'/' + ISO_code + '/' + ISO_code_l + '_ppp_2020_constrained.tif'

        #Check if country already in imagery list, if yes don't download
        if row['Name'] in imagery:

            print('already downloaded')

        else: 

            urlretrieve(filepath, r'/data/population_surface_copy.tif')    
            imagery.append(row['Name'])
        
        
        # get the country from the world shapefile
        country = world.loc[(world.ISO_A3 == ISO_code)]
    
        #get population of the country
        population = row['population']
    
        #Sample point for every 100,000 people
        #Round up 
        no_samples = ceil(population / 100000)
    
        #Get geometry
        country_geom = country.geometry.iloc[0]
                
        #Project so can calculate area
        project = country.to_crs("+proj=eqearth")
    
        print("Number of samples: " + str(no_samples))

        #count how  many network features
        numFeatures = 0

        for c in classes: 
            numFeatures += int(count_network(c, country_geom))

        
        #Set up iterator
        n = 1
        
        #Open population data            
        with rio_open('../data/' + str(ISO_code_l) + '.tif') as src:
            
            # get tranformation matrix
            a = src.transform
            
            # extract band 1 (only band)
            d = src.read(1)
            
            # calculate the offset (from tl to centre of cell)
            offset = src.res[0] / 2
            
            #Repeat 100 times
            while n < 101:
                
                #initiliase empty list to store coordinates 
                coords = []

                #Initialise counter to see how many points made 
                n_points = 0 

                # for n samples
                while n_points < no_samples:
                    
                    # create a random point within the bounds of the country
                    seed = Point([uniform(src.bounds[0], src.bounds[2]),
                                uniform(src.bounds[1], src.bounds[3])])
                    
                    # convert seed to image space
                    s_img = src.index(seed.x, seed.y)
                    
                    # get population value
                    pop_val = d[s_img]
                    
                    # make sure +ve population
                    if pop_val > 0: 
                        
                        #get x y coordinates of the image
                        coords.append(src.xy(s_img[0],s_img[1]))
                        
                        #add to counter
                        n_points += 1
                    
                #list to store mean distance calculated for each level of completeness
                cost_degradation = []
                cost_degradation.append(row['Name'])

                #split population dataset into 4 
                data = [coords[i::NUM_THREADS] for i in range(NUM_THREADS)]

                #While dataset has features in it
                while numFeatures >= 0:
                    
                    #Initialise empty list to store costs
                    costs = []
                    
                    #routing on database using 4 threads
                    with Pool(NUM_THREADS) as p: 
                        distances_2d, network, comparison = zip(*p.map(routing, data))
    
                    #flatten distances 
                    distances = [d for dist in distances_2d for d in dist]

                    #get the mean distance for each point
                    mean_dist = sum(distances)/len(distances)
                    print("Mean distance to healthcare: " + str(mean_dist)) # remove at runtime if not needed
                    cost_degradation.append(mean_dist)
                
                    #initialise degradation value as null
                    degrade_val = NULL
        
                    # degrade each class
                    for c in classes: 
                        degrade_val = degrade_values(c, country_geom, degradationVal)
                    
                    #Degrade dataset coverage value by chosen degradation percentage
                    coverage -= degrade_val 

                #work out how many blank results you have 
                results_blank = 12 - len(cost_degradation)

                #initiliase an iterator
                i = 0

                #while iterator less than amount of blanks
                while i < results_blank:

                    #Add N/A to list so that there are values for each completeness
                    cost_degradation.append('N/A')
                    i += 1
                    
                #add mean distance results to the country results
                country_results.append(cost_degradation)

                #reset nodes
                connection = pg.connect(database="routing_world", user="postgres", password="", host="127.0.0.1", port="5432")
                cur = connection.cursor()
                cur.execute("UPDATE wo_2po_4pgr SET source_2 = source where source_2 is null;")
                cur.execute("UPDATE wo_2po_4pgr SET target_2 = target where target_2 is null;")
            
                #close database connection 
                connection.commit()
                connection.close()
                
                n +=1


#Write outputs to CSV
#Distances measured on degraded network
dataframe = DataFrame(country_results, columns=['Country', '100', '90','80','70', '60','50', '40', '30', '20', '10', '0'])
dataframe.to_csv('./data/results_2023_2.csv', index=False)
