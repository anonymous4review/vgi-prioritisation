Setup data and tmp files
```
mkdir /data/osm
udo chown -R postgres:postgres /data/osm/ 
mkdir /data/tmp_usr
sudo chown -R postgres:postgres /data/tmp_usr/
```

Install & set postgres to new location (whilst setting the data directory, also tune the memory access)
```
sudo apt install postgresql-pgroutingsudo passwd postgres
sudo service postgresql stopsudo -u postgres /usr/lib/postgresql/12/bin/pg_ctl -D /data/osm/ -l /data/psql_logfile_user.log start
sudo -u postgres /usr/lib/postgresql/12/bin/pg_ctl -D /data/osm/ -l /data/psql_logfile_user.log stop
sudo -u postgres nano /etc/postgresql/12/main/postgresql.conf (data_directory)
sudo service postgresql start
```

Get data
```
wget http://download.geofabrik.de/laos-latest.osm.pbf
```

Edit config file to set tag types and activate psql output! (see here)
```
nano osm2po/osm2po.config
```

Extract to SQL file
```
nohup java -Xmx24g -jar osm2po/osm2po-core-5.5.1-signed.jar prefix=wo tilesize=x 
laos-latest.osm.pbf postp.0.class.de.cm.osm2po.plugins.postp.PgRoutingWriter &
```

Load into db
```
sudo -u postgres createdb routingcreate extension postgis;
create extension pgrouting;
create extension hstore;
sudo -u postgres routing -f wo/wo_2po_4pgr.sql
```

Build Vertices table
```
select pgr_createVerticesTable('sa_2po_4pgr', 'geom_way');
```

Rebuild topology (not necessary - it makes no difference)
```
select pgr_createTopology('sa_2po_4pgr', 0.0001, 'geom_way', clean:=true);
```

Analyse Graph (not necessary as it doesn't help - it just reports any issues)
```
select pgr_analyzeGraph('sa_2po_4pgr', 0.0001, 'geom_way');
```

If you want to make costs more meaningful, convert degrees to metres
```
update sa_2po_4pgr set cost = ST_LengthSpheroid(geom_way, 'SPHEROID["WGS_1984",6378137,298.257223563]');
```

Find out data type of a column 
```
SELECT data_type FROM information_schema.columns WHERE table_name = 'wo_2po_4pgr' and column_name ='source';
```

Add extra source and target columns to work on so you have original data stored in the table
```
alter table wo_2po_4pgr add column source_2 integer; 
alter table wo_2po_4pgr add column target_2 integer;
update wo_2po_4pgr set source_2 = source;
update wo_2po_4pgr set target_2 = target; 
alter table wo_2po_4pgr add column class2 varchar;
```

Create indexes on source and target columns (not sure if multicolumn one is required...)
```
create index on wo_2po_4pgr(source);
create index on wo_2po_4pgr(target);
create index on wo_2po_4pgr(source, target);
```

Filter nodes for primary healthcare:
https://wiki.openstreetmap.org/wiki/Key:healthcare
https://wiki.openstreetmap.org/wiki/Key:amenity#Healthcare
https://wiki.openstreetmap.org/wiki/Global_Healthsites_Mapping_Project
```
JAVACMD_OPTIONS="-D java.io.tmp dir=/data/tmp_usr" nohup ../osmosis/bin/osmosis --read-pbf file=laos-latest.osm.pbf --tf accept-nodes amenity=clinic,doctors,hospital --tf accept-nodes healthcare=clinic,doctor,hospital --tf reject-relations --tf reject-ways --write-pbf primary_healthcare.pbf &
```

Can check progress with:
```
ps aux | grep osmosis
```
or
```
tail nohup.out
```

Load into database table (set -C in MB > the size of the pbf, or 75% of RAM if you can't accommodate that)
```
sudo -u postgres osm2pgsql -d routing -s -C 2048 -l -c primary_healthcare.pbf
```

Finally, tidy indices etc
```
vacuum analyze;
```

Or, a much longer-winded version that will lock the db but might have some benefits:
```
vacuum full analyze;
```
