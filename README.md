# VGI Prioritisation

This repository is provided with the manuscript *"Maximising the value of a volunteer: A novel method for prioritising humanitarian VGI activities"* (submitted to [JOSIS](https://josis.org/index.php/josis)) to promote replicability and facilitate the review process.

## Summary

- Script developed to assist the prioritisation of mapping completed by volunteer humanitarian mapping organisations. 
- A spatial analysis of choice is run on an increasingly degraded dataset to measure the impact of further volunteer contributions on the desired use of the map data. The script is designed to be run on several candidate countries to identify which country(s) should be prioritised. 
- In this script measuring distance to healthcare along road and path networks is used as an example use case of map data. The script outputs the change in distance to healthcare with decreasing number of road network features. The country where change in distance to healthcare at the current level of network coverage should be prioritised. 


## Dependencies

Requires the following libraries for database set up and to run the script: 
- osm2po (https://osm2po.de/)
- PostGIS (https://postgis.net/)
- osm2pgsql (https://osm2pgsql.org/)
- pgRouting (https://pgrouting.org/)
- Python 3 ([https://www.python.org/](https://www.python.org/))

The Python script requires the following libraries: 
- fiona
- rasterio
- shapely
- numpy
- geopandas
- pandas
- pyscopg2
- pyproj
- urllib
- multiprocessing

## Setup
To set up the database, use the instructions in [database.md](database.md)

## Acknowledgements

The `data/ne_10m_admin_0_countries.*` dataset is provided for convenience - it available in the public domain from: [Natural Earth](https://www.naturalearthdata.com/). More details can be found in the included [README](data/ne_10m_admin_0_countries.README.html) file.
